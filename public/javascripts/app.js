   function displayAlertSuccess(message) {
        return "<div class=\"alert alert-success alert-dismissible\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button><h4><i class=\"icon fa fa-check\"></i>Opération effectuée</h4>"+message+"</div>";
    }
    function displayAlertError(message) {
        return "<div class=\"alert alert-danger alert-dismissible\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button><h4><i class=\"icon fa fa-check\"></i>Opération echouée</h4>"+message+"</div>";
    }
    function displayAlertWarning(message) {
        return "<div class=\"alert alert-warning alert-dismissible\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button><h4><i class=\"icon fa fa-check\"></i>Opération echouée</h4>"+message+"</div>";
    }

    function pushNotification(urlString){
             $.ajax({
                            type: "GET",
                            url: urlString,

                            dataType: "JSON",
                            success: function (data, status)
                            {
                              console.log(data);
                              $('#successEventModal').modal('show');

                            },
                            error: function (xhr, desc, err)
                            {
                                $('#failureEventModal').modal('show');

                            }
                 });

    }

  $(function () {

      //Date range picker with time picker
      $('#eventTimeInput').daterangepicker({timePicker: true, timePickerIncrement: 30,  locale: {
              format: 'MM/DD/YYYY h:mm A'
          }});


    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });






    //bootstrap WYSIHTML5 - text editor
    $(".textarea").wysihtml5();



      $('#add-enseignements-video').on('click',function(){

            $('#enseignementVideoModal').modal('show');

      });

      $('#add-enseignements-audio').on('click',function(){

            $('#enseignementAudioModal').modal('show');

      });

       $('#add-event').on('click',function(){

            $('#eventModal').modal('show');

      });

       $('#broadcast-live').on('click',function(){

            $('#liveModal').modal('show');

      });
       $('#add-verse').on('click',function(){

            $('#verseModal').modal('show');

      });

        $('#add-message').on('click',function(){

            $('#messageModal').modal('show');

      });

    $('#push-notification-enseignements').click(function(event){
            event.stopPropagation();
            var url = $('#push-notification-enseignements').attr('href');
            pushNotification(url);

      });

       $('#push-notification-events').click(function(event){
            event.stopPropagation();
            var url = $('#push-notification-events').attr('href');
            pushNotification(url);

      });

       $('#push-notification-verses').click(function(event){
                  event.stopPropagation();
                  var url = $('#push-notification-verses').attr('href');
                  pushNotification(url);

            });

     $('#push-notification-messages').click(function(event){
                event.stopPropagation();
                var url = $('#push-notification-messages').attr('href');
                pushNotification(url);

          });


       $('#add-verse-create').click(function (event) {
            var titleVerse = $('#titreVerseInput').val();
            var sousTitreVerse = $('#sousTitreVerseInput').val();
            var contentVerse = $('#contentVerseInput').val();
            var authorNameVerse = $('#authorNameVerseInput').val();
            var authorPhotoVerse = $('#authorPhotoVerseInput').val();
            var churchSessionVerse = $('#churchSessionVerseInput').val();

                $.ajax({
                            type: "POST",
                            url: "/api/v1/admin/verses/create",
                            data: {"titre": titleVerse,
                                "sousTitre":sousTitreVerse,
                                "description":contentVerse,
                                "authorPhoto": authorPhotoVerse,
                                "authorName":authorNameVerse,
                                "churchSession":churchSessionVerse

                               },
                            dataType: "JSON",
                            success: function (data, status)
                            {
                               $('#messageVerseModal').empty();
                               $('#messageVerseModal').append(displayAlertSuccess("Opération effectuée avec succès"));

                            },
                            error: function (xhr, desc, err)
                            {
                                $('#messageVerseModal').empty();
                                $('#messageVerseModal').append(displayAlertError("Error Rencontrée"));

                            }
                 });

       });

        $('#add-message-create').click(function (event) {
            var titleMessage = $('#titreMessageInput').val();
            var contentMessage = $('#contentMessageInput').val();


                $.ajax({
                            type: "POST",
                            url: "/api/v1/admin/messages/create",
                            data: {"titre": titleMessage,
                                   "content":contentMessage

                               },
                            dataType: "JSON",
                            success: function (data, status)
                            {
                               $('#messageMessageModal').empty();
                               $('#messageMessageModal').append(displayAlertSuccess("Opération effectuée avec succès"));

                                  $('#titreMessageInput').val('');
                                  $('#contentMessageInput').val('');
                            },
                            error: function (xhr, desc, err)
                            {
                                $('#messageMessageModal').empty();
                                $('#messageMessageModal').append(displayAlertError("Error Rencontrée"));

                            }
                 });

       });

       $('#add-live-create').click(function (event) {
            var titleLive = $('#titreLiveInput').val();
            var youtubeId = $('#youtubeIdLiveInput').val();
            var linkLive= $('#linkLiveInput').val();

                $.ajax({
                            type: "POST",
                            url: "/api/v1/admin/live/edit",
                            data: {"titre": titleLive,
                                "youtubeId":youtubeId,
                                "linkLive":linkLive

                               },
                            dataType: "JSON",
                            success: function (data, status)
                            {
                               $('#messageLiveModal').empty();
                               $('#messageLiveModal').append(displayAlertSuccess("Broadcast effectué avec succès"));

                            },
                            error: function (xhr, desc, err)
                            {
                                $('#messageLiveModal').empty();
                                $('#messageLiveModal').append(displayAlertError("Error Rencontrée"));

                            }
                 });

       });

        $('#add-event-create').click(function (event) {
            var titleEvent = $('#titreEventInput').val();
            var imageEvent = $('#imageEventInput').val();
            var descriptionEvent = $('#descriptionEventInput').val();
            var linkEvent = $('#linkEventInput').val();
            var dateRangeEvent = $('#eventTimeInput').val();

                $.ajax({
                            type: "POST",
                            url: "/api/v1/admin/events/create",
                            data: {"titre": titleEvent,
                                "description":descriptionEvent,
                                "image":imageEvent,
                                "link":linkEvent,
                                "dateRangeEvent":dateRangeEvent
                            },
                            dataType: "JSON",
                            success: function (data, status)
                            {
                               $('#messageEventModal').empty();
                               $('#messageEventModal').append(displayAlertSuccess("Ajouté avec succès"));
                              $('#titreEventInput').val('');

                               $('#imageEventInput').val('');

                               $('#descriptionEventInput').val('');
                               $('#linkEventInput').val('');

                            },
                            error: function (xhr, desc, err)
                            {
                                $('#messageEventModal').empty();
                                $('#messageEventModal').append(displayAlertError("Error Rencontrée"));

                            }
                 });

       });

      $('#add-enseignements-audio-create').click(function (event) {
            var titleAudio = $('#titreAudioInput').val();
            var subTitleAudio = $('#sousTitreAudioInput').val();
            var imageAudio = $('#imageAudioInput').val();
            var typeEnsAudio = $('#typeEnsAudioInput').val();
            var descriptionAudio = $('#descriptionAudioInput').val();
            var linkAudio = $('#linkAudioInput').val();
            var auteurAudio = $('#auteurAudioInput').val();

                $.ajax({
                            type: "POST",
                            url: "/api/v1/admin/enseignements/create",
                            data: {"titre": titleAudio,
                                "sousTitre": subTitleAudio,
                                "description":descriptionAudio,
                                "image":imageAudio,
                                "link":linkAudio,
                                "auteur":auteurAudio,
                                "typeEnseignement":typeEnsAudio
                            },
                            dataType: "JSON",
                            success: function (data, status)
                            {
                               $('#messageEnseignementAudioModal').empty();
                               $('#messageEnseignementAudioModal').append(displayAlertSuccess("Ajouté avec succès"));
                              $('#titreAudioInput').val('');

                               $('#sousTitreAudioInput').val('');
                               $('#imageAudioInput').val('');

                               $('#descriptionAudioInput').val('');
                               $('#linkAudioInput').val('');

                            },
                            error: function (xhr, desc, err)
                            {
                                $('#messageEnseignementAudioModal').empty();
                                $('#messageEnseignementAudioModal').append(displayAlertError("Error Rencontrée"));

                            }
                 });

       });

         $('#add-enseignements-video-create').click(function (event) {

            var titleVideo = $('#titreVideoInput').val();
            var subTitleVideo = $('#sousTitreVideoInput').val();
            var imageVideo = $('#imageVideoInput').val();
            var typeEnsVideo = $('#typeEnsVideoInput').val();
            var descriptionVideo = $('#descriptionVideoInput').val();
            var linkVideo = $('#linkVideoInput').val();
            var auteurVideo = $('#auteurVideoInput').val();

                $.ajax({
                            type: "POST",
                            url: "/api/v1/admin/enseignements/create",
                            data: {"titre": titleVideo,
                                "sousTitre": subTitleVideo,
                                "description":descriptionVideo,
                                "image":imageVideo,
                                "link":linkVideo,
                                "auteur":auteurVideo,
                                "typeEnseignement":typeEnsVideo
                            },
                            dataType: "JSON",
                            success: function (data, status)
                            {
                               $('#messageEnseignementVideoModal').empty();
                               $('#messageEnseignementVideoModal').append(displayAlertSuccess("Ajouté avec succès"));
                               $('#titreVideoInput').val('');

                               $('#sousTitreVideoInput').val('');
                               $('#imageVideoInput').val('');

                               $('#descriptionVideoInput').val('');
                               $('#linkVideoInput').val('');
                            },
                            error: function (xhr, desc, err)
                            {
                                $('#messageEnseignementVideoModal').empty();
                                $('#messageEnseignementVideoModal').append(displayAlertError("Erreur rencontrée"));

                            }
                 });


         });

  });