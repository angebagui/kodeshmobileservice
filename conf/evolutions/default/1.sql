# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table admin (
  id                            bigint auto_increment not null,
  email                         varchar(255),
  password                      varchar(255),
  constraint pk_admin primary key (id)
);

create table device (
  id                            bigint auto_increment not null,
  token                         varchar(255),
  user_id                       bigint,
  created_at                    bigint,
  constraint pk_device primary key (id)
);

create table evenements (
  id_evenement                  bigint auto_increment not null,
  libelle                       varchar(255),
  description                   varchar(255),
  image                         varchar(255),
  date_debut                    datetime(6),
  date_fin                      datetime(6),
  heure_debut                   varchar(255),
  heure_fin                     varchar(255),
  activer                       tinyint(1) default 0,
  mots_cles                     varchar(255),
  date_publication              datetime(6),
  date_enreg                    datetime(6),
  user_enreg                    integer,
  user_maj                      integer,
  constraint pk_evenements primary key (id_evenement)
);

create table information (
  id                            varchar(255) not null,
  title                         varchar(255),
  content                       varchar(255),
  created                       bigint,
  constraint pk_information primary key (id)
);

create table librairies (
  id_librairie                  bigint auto_increment not null,
  code_cat                      varchar(255),
  libelle                       varchar(255),
  description                   varchar(255),
  date_publication              datetime(6),
  devise                        varchar(255),
  prix                          double,
  affiche_prix                  tinyint(1) default 0,
  auteur                        varchar(255),
  image                         varchar(255),
  mots_cles                     varchar(255),
  lies                          varchar(255),
  reference_prod                varchar(255),
  activer                       tinyint(1) default 0,
  nbre_vus                      integer,
  cumul_vote                    integer,
  nbre_vote                     integer,
  derniere_vus                  datetime(6),
  date_enreg                    datetime(6),
  date_maj                      datetime(6),
  user_enreg                    integer,
  user_maj                      integer,
  themes                        varchar(255),
  constraint pk_librairies primary key (id_librairie)
);

create table live (
  id                            integer auto_increment not null,
  title                         varchar(255),
  youtube_id                    varchar(255),
  url                           varchar(255),
  created                       bigint,
  constraint pk_live primary key (id)
);

create table paroles_vie (
  id_parole                     bigint auto_increment not null,
  id_type                       integer,
  libelle                       varchar(255),
  libelle2                      varchar(255),
  description                   varchar(255),
  image                         varchar(255),
  fichier                       varchar(255),
  url_media                     varchar(255),
  duree                         varchar(255),
  auteur                        varchar(255),
  mots_cles                     varchar(255),
  lies                          varchar(255),
  activer                       tinyint(1) default 0,
  nbre_vus                      integer,
  code_cat                      varchar(255),
  date_publication              datetime(6),
  date_enreg                    datetime(6),
  date_maj                      datetime(6),
  user_enreg                    integer,
  user_maj                      integer,
  constraint pk_paroles_vie primary key (id_parole)
);

create table user (
  id                            bigint auto_increment not null,
  email                         varchar(255),
  name                          varchar(255),
  constraint pk_user primary key (id)
);

create table verset (
  id                            bigint auto_increment not null,
  title                         varchar(255),
  subtitle                      varchar(255),
  content                       varchar(255),
  authorname                    varchar(255),
  authorphoto                   varchar(255),
  isread                        tinyint(1) default 0,
  created                       bigint,
  church_session                varchar(255),
  constraint pk_verset primary key (id)
);

create table zoom_sur (
  id_zoom                       bigint auto_increment not null,
  code_zoom                     varchar(255),
  libelle                       varchar(255),
  description                   varchar(255),
  image                         varchar(255),
  constraint pk_zoom_sur primary key (id_zoom)
);


# --- !Downs

drop table if exists admin;

drop table if exists device;

drop table if exists evenements;

drop table if exists information;

drop table if exists librairies;

drop table if exists live;

drop table if exists paroles_vie;

drop table if exists user;

drop table if exists verset;

drop table if exists zoom_sur;

