name := """play-java-intro"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava, PlayEbean)

scalaVersion := "2.11.8"

herokuAppName in Compile := "kodeshmobileservice"

libraryDependencies ++= Seq(
  javaJdbc,
  javaWs,
  "org.mockito" % "mockito-core" % "2.1.0",
  javaWs % "test",
 // "org.hibernate" % "hibernate-core" % "5.2.5.Final"
  "mysql" % "mysql-connector-java" % "6.0.5"

)
