package actors.message;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.Enseignement;
import play.libs.Json;

/**
 * Created by angebagui on 22/01/2017.
 */
public class LearningEvent {

    private ObjectNode jsonNode;

    private Enseignement enseignement;

    public LearningEvent() {
    }

    public LearningEvent(Enseignement enseignement) {
        this.enseignement = enseignement;
        jsonNode = Json.newObject();
        jsonNode.put("id_enseignement", enseignement.id);
        jsonNode.put("title", enseignement.title);
        jsonNode.put("subTitle", enseignement.subTitle);
        jsonNode.put("image", enseignement.image);
    }


    public JsonNode getJsonNode() {
        return jsonNode;
    }

    public void setJsonNode(ObjectNode jsonNode) {
        this.jsonNode = jsonNode;
    }
}
