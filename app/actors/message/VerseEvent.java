package actors.message;

import com.fasterxml.jackson.databind.node.ObjectNode;
import models.Verse;
import play.libs.Json;

/**
 * Created by angebagui on 23/01/2017.
 */
public class VerseEvent {

    private Verse verse;

    private ObjectNode jsonNode;

    public VerseEvent(Verse verse) {
        this.verse = verse;
    jsonNode = Json.newObject();
    jsonNode.put("id_verse", this.verse.id);
        jsonNode.put("title", this.verse.title);
        jsonNode.put("subTitle", this.verse.subTitle);

    }

    public Verse getVerse() {
        return verse;
    }

    public void setVerse(Verse verse) {
        this.verse = verse;
    }

    public ObjectNode getJsonNode() {
        return jsonNode;
    }

    public void setJsonNode(ObjectNode jsonNode) {
        this.jsonNode = jsonNode;
    }
}
