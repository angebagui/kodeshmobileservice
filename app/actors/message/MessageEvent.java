package actors.message;

import com.fasterxml.jackson.databind.node.ObjectNode;
import models.Message;
import play.libs.Json;

/**
 * Created by angebagui on 23/01/2017.
 */
public class MessageEvent {

    private Message message;

    private ObjectNode jsonNode;


    public MessageEvent(Message message) {
        this.message = message;
        jsonNode = Json.newObject();
        jsonNode.put("id_message", message.getId());
        jsonNode.put("title", message.getTitle());
        jsonNode.put("content", message.getContent());
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public ObjectNode getJsonNode() {
        return jsonNode;
    }

    public void setJsonNode(ObjectNode jsonNode) {
        this.jsonNode = jsonNode;
    }
}
