package actors.message;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.Evenement;
import play.libs.Json;

/**
 * Created by angebagui on 22/01/2017.
 */
public class AgendaEvent {

    private Evenement evenement;
    private ObjectNode jsonNode;

    public AgendaEvent(Evenement evenement) {
        this.evenement = evenement;
        jsonNode = Json.newObject();
        jsonNode.put("title", evenement.title);
        jsonNode.put("id_evenement", evenement.id);
        jsonNode.put("image", evenement.image);

    }

    public AgendaEvent() {

    }

    public Evenement getEvenement() {
        return evenement;
    }

    public void setEvenement(Evenement evenement) {
        this.evenement = evenement;
    }

    public ObjectNode getJsonNode() {
        return jsonNode;
    }

    public void setJsonNode(ObjectNode jsonNode) {
        this.jsonNode = jsonNode;
    }
}
