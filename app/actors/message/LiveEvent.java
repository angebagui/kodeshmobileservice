package actors.message;

import com.fasterxml.jackson.databind.node.ObjectNode;
import models.Live;
import play.libs.Json;

/**
 * Created by angebagui on 22/01/2017.
 */
public class LiveEvent {

    private ObjectNode jsonNode;
    private Live mLive;

    public LiveEvent(Live live) {
        this.mLive = live;

        jsonNode = Json.newObject();
        jsonNode.put("youtubeId", this.mLive.youtubeId);
        jsonNode.put("title", this.mLive.title);

    }

    public ObjectNode getJsonNode() {
        return jsonNode;
    }
}
