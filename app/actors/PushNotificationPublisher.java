package actors;

import actors.message.*;
import akka.actor.Props;
import akka.actor.UntypedActor;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.Device;
import play.Logger;
import play.libs.Json;
import play.libs.ws.WSResponse;
import services.GcmService;
import play.libs.ws.WSClient;

import javax.inject.Inject;
import java.util.function.Function;

/**
 * Created by angebagui on 22/01/2017.
 */
public class PushNotificationPublisher extends UntypedActor{

    private WSClient ws;

    @Inject
    public PushNotificationPublisher(WSClient ws) {
        this.ws = ws;
    }

    @Override
    public void onReceive(Object message) throws Throwable {

        if(message instanceof LearningEvent){
            ObjectNode data = Json.newObject();
            data.put("type", "enseignement");
            data.put("data", ((LearningEvent) message).getJsonNode());

            for(Device device: Device.finder.findList()){
                if(device.token != null){
                    GcmService.sendPayloadWithData(this.ws, data,device.token).thenApplyAsync(new Function<WSResponse, Void>() {
                        @Override
                        public Void apply(WSResponse wsResponse) {
                            System.out.println(wsResponse.asJson());

                            return null;
                        }
                    });
                }

            }


        }else if(message instanceof AgendaEvent){
            ObjectNode data = Json.newObject();
            data.put("type", "event");
            data.put("data", ((AgendaEvent) message).getJsonNode());
            for(Device device: Device.finder.findList()){
                GcmService.sendPayloadWithData(this.ws, data,device.token).thenApplyAsync(new Function<WSResponse, Void>() {
                    @Override
                    public Void apply(WSResponse wsResponse) {
                        System.out.println(wsResponse.asJson());

                        return null;
                    }
                });
            }

        }else if(message instanceof LiveEvent){

            ObjectNode data = Json.newObject();
            data.put("type", "live");
            data.put("data", ((LiveEvent) message).getJsonNode());
            for(Device device: Device.finder.findList()){
                Logger.debug("Message Live Event "+message);


                GcmService.sendPayloadWithData(this.ws, data,device.token).thenApplyAsync(new Function<WSResponse, Void>() {
                    @Override
                    public Void apply(WSResponse wsResponse) {
                            System.out.println(wsResponse.getBody());

                        return null;
                    }
                });
            }

        } else if(message instanceof VerseEvent){
            ObjectNode data = Json.newObject();
            data.put("type", "verse");
            data.put("data", ((VerseEvent) message).getJsonNode());
            for(Device device: Device.finder.findList()){

                GcmService.sendPayloadWithData(this.ws, data,device.token).thenApplyAsync(new Function<WSResponse, Void>() {
                    @Override
                    public Void apply(WSResponse wsResponse) {
                        System.out.println(wsResponse.getBody());

                        return null;
                    }
                });
            }
        } else if( message instanceof MessageEvent){

            ObjectNode data = Json.newObject();
            data.put("type", "message");
            data.put("data", ((MessageEvent) message).getJsonNode());
            for(Device device: Device.finder.findList()){


                GcmService.sendPayloadWithData(this.ws, data,device.token).thenApplyAsync(new Function<WSResponse, Void>() {
                    @Override
                    public Void apply(WSResponse wsResponse) {
                        System.out.println(wsResponse.getBody());

                        return null;
                    }
                });
            }


        }else {
            unhandled(message);
        }

    }

    @Override
    public void unhandled(Object message) {
        super.unhandled(message);
    }
}
