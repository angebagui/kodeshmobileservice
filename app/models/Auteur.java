package models;

import com.avaje.ebean.Model;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by angebagui on 22/01/2017.
 */
@Entity(name = "zoom_sur")
@Table(name = "zoom_sur")
public class Auteur extends Model {

    @Id
    @Column(name = "id_zoom")
    public Long id;

    @Column(name = "code_zoom")
    public String code;

    @Column(name = "libelle")
    public String name;

    @Column(name = "description")
    public String bio;

    @Column(name = "image")
    public String image;

    public static Finder<Long, Auteur> finder = new Finder<Long, Auteur>(Auteur.class);

    @JsonProperty("id_zoom")
    public Long getId() {
        return id;
    }

    @JsonProperty("code_zoom")
    public String getCode() {
        return code;
    }

    @JsonProperty("libelle")
    public String getName() {
        return name;
    }

    @JsonProperty("description")
    public String getBio() {
        return bio;
    }
}
