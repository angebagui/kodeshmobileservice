package models;

import com.avaje.ebean.Model;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.DateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Date;

/**
 * Created by angebagui on 24/01/2017.
 */
@Entity( name = "librairies")
@Table(name = "librairies")
public class Librairie extends Model {

    @Id
    @Column(name = "id_librairie")
    public Long id_librairie;

    public String code_cat;

    public String libelle;

    public String description;

    //@JsonSerialize(using = DateToLongSerializer.class, as = Long.class)
    @Column(name = "date_publication")
    public Date date_publication;

    public String devise;

    public Double prix;

    public Boolean affiche_prix;

    public String auteur;

    public String image;

    public String mots_cles;

    public String lies;

    public String reference_prod;

    public Boolean activer;

    public Integer nbre_vus;

    public Integer cumul_vote;

    public Integer nbre_vote;

    //@JsonSerialize(using = DateTimeToLongSerializer.class, as = Long.class)
    @Column(name = "derniere_vus")
    public DateTime derniere_vus;

    //@JsonSerialize(using = DateTimeToLongSerializer.class, as = Long.class)
    @Column(name = "date_enreg")
    public DateTime date_enreg;

    //@JsonSerialize(using = DateTimeToLongSerializer.class, as = Long.class)
    @Column(name = "date_maj")
    public DateTime date_maj;

    @Column(name = "user_enreg")
    public Integer user_enreg;

    public Integer user_maj;


    public String 	themes;

    public static Finder<Long, Librairie> finder = new Finder<Long, Librairie>(Librairie.class);

    @JsonProperty
    public Long getDate_publication() {
        return  date_publication != null? date_publication.getTime():0L;
    }

    @JsonProperty
    public Long getDerniere_vus() {
        return derniere_vus != null?derniere_vus.getMillis():0L;
    }

    @JsonProperty
    public Long getDate_enreg() {
        return date_enreg != null ? date_enreg.getMillis():0L;
    }

    @JsonProperty
    public Long getDate_maj() {
        return date_maj!=null?date_maj.getMillis():0L;
    }
}
