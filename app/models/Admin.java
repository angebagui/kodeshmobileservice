package models;

import com.avaje.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by angebagui on 14/01/2017.
 */
@Entity
public class Admin extends Model{

    @Id
    public Long id;

    public String email;
    public String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public static Finder<Long, Admin> finder = new Finder<Long, Admin>(Admin.class);

}
