package models;

import com.avaje.ebean.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by angebagui on 22/01/2017.
 */
@Entity(name = "device")
@Table(name = "device")
public class Device extends Model {

    @Id
    @Column(name = "id")
    public Long id;

    @Column(name = "token")
    public String token;

    @Column(name = "user_id")
    public Long userId;

    @Column(name = "created_at")
    public Long created_at;

    public static Finder<Long, Device> finder = new Finder<Long, Device>(Device.class);


}
