package models;

import com.avaje.ebean.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by angebagui on 15/01/2017.
 */
@Entity(name = "live")
@Table(name = "live")
public class Live extends Model {

    @Id
    public Integer id;

    @Column(name = "title")
    public String title;

    @Column(name = "youtube_id")
    public String youtubeId;

    @Column(name = "url")
    public String url;


    public Long created;

    public static Finder<Integer, Live> finder = new Finder<Integer, Live>(Live.class);
}
