package models;

import com.avaje.ebean.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by angebagui on 23/01/2017.
 */
@Entity(name = "verset")
@Table(name = "verset")
public class Verse extends Model {

    @Id
    @Column(name = "id")
    public Long id;

    @Column(name = "title")
    public String title;

    @Column(name = "subtitle")
    public String subTitle;

    @Column(name = "content")
    public String content;

    @Column(name = "authorName")
    public String authorName;

    @Column(name = "authorPhoto")
    public String authorPhoto;

    @Column(name = "isRead")
    public Boolean isRead;

    @Column(name = "created")
    public Long created;

    @Column(name = "church_session")
    public String churchSession;

    public static Finder<Long, Verse> finder = new Finder<Long, Verse>(Verse.class);

}
