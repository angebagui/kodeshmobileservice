package models;

import com.avaje.ebean.Model;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by angebagui on 15/01/2017.
 */
@Entity(name = "paroles_vie")
@Table(name = "paroles_vie")
public class Enseignement extends Model {

    @Id
    @Column(name = "id_parole")
    public Long id;

    @Column(name = "id_type")
    public Integer idType;

    @Column(name = "libelle")
    public String title;

    @Column(name = "libelle2")
    public String subTitle;

    @Column(name = "description")
    public String description;

    @Column(name = "image")
    public String image;

    @Column(name = "fichier")
    public String fichier;

    @Column(name = "url_media")
    public String urlMedia;

    public String duree;

    @JsonIgnoreProperties
    public  String auteur;

    public String motsCles;

    public String lies;

    @Column(name = "activer")
    public Boolean activer;

    public Integer nbreVus;

    public String code_cat;

    @Column(name = "date_publication")
    public Date publishedAt;

    @Column(name = "date_enreg")
    public Date created;

    @Column(name = "date_maj")
    public Date updated;

    @Column(name = "user_enreg")
    public Integer creator;

    @Column(name = "user_maj")
    public Integer updater;

    @Transient
    public Auteur auteurField;


    @JsonProperty("auteur")
    public Auteur getAuteurField() {
        return Auteur.finder.where().eq("code_zoom", this.auteur).findUnique();
    }


    @JsonProperty("date_publication")
    public Date getPublishedAt() {
        return publishedAt;
    }

    @JsonProperty("url_media")
    public String getUrlMedia() {
        return urlMedia;
    }

    @JsonProperty("date_enreg")
    public Date getCreated() {
        return created;
    }

    @JsonProperty("date_maj")
    public Date getUpdated() {
        return updated;
    }

    @JsonProperty("id_type")
    public Integer getIdType() {
        return idType;
    }

    @JsonProperty("libelle")
    public String getTitle() {
        return title;
    }

    @JsonProperty("libelle2")
    public String getSubTitle() {
        return subTitle;
    }

    @JsonProperty("user_enreg")
    public Integer getCreator() {
        return creator;
    }

    @JsonProperty("user_maj")
    public Integer getUpdater() {
        return updater;
    }

    @JsonProperty("id_parole")
    public Long getId() {
        return id;
    }

    public static Finder<Long, Enseignement> finder = new Finder<Long, Enseignement>(Enseignement.class);
}
