package models;

import com.avaje.ebean.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by angebagui on 23/01/2017.
 */
@Table(name = "information")
@Entity(name = "information")
public class Message extends Model {

    @Id
    @Column(name = "id")
    public String id;
    public String title;
    public String content;

    public Long created;
    public Message() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public static Finder<Long, Message> finder = new Finder<Long, Message>(Message.class);
}
