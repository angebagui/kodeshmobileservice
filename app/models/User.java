package models;

import com.avaje.ebean.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by angebagui on 14/01/2017.
 */
@Table(name = "user")
@Entity(name = "user")
public class User extends Model{

    @Id
    @Column(name = "id")
    public Long id;

    public String email;

    public String name;



}
