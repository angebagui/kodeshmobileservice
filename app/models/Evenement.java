package models;

import com.avaje.ebean.Model;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;


/**
 * Created by angebagui on 15/01/2017.
 */
@Entity(name = "evenements")
@Table(name = "evenements")
public class Evenement extends Model {

    @Id
    @Column(name = "id_evenement")
    public Long id;

    @Column(name = "libelle")
    public String title;

    @Column(name = "description")
    public String description;

    @Column(name = "image")
    public String image;

    @Column(name = "date_debut")
    public Date startTime;

    @Column(name = "date_fin")
    public Date endTime;

    @Column(name = "heure_debut")
    public String heureDebut;

    @Column(name = "heure_fin")
    public String heureFin;

    public Boolean activer;

    public String motsCles;

    @Column(name = "date_publication")
    public Date publishedAt;

    @Column(name = "date_enreg")
    public Date created;

    //@Column(name = "date_maj")
    //public Date updated;

    @Column(name = "user_enreg")
    public Integer creator;

    @Column(name = "user_maj")
    public Integer updater;

    public static Finder<Long, Evenement> finder = new Finder<Long, Evenement>(Evenement.class);

    @JsonProperty("id_evenement")
    public Long getId() {
        return id;
    }

    @JsonProperty("libelle")
    public String getTitle() {
        return title;
    }

    @JsonProperty("date_debut")
    public Date getStartTime() {
        return startTime;
    }
    @JsonProperty("date_fin")
    public Date getEndTime() {
        return endTime;
    }

    @JsonProperty("heure_debut")
    public String getHeureDebut() {
        return heureDebut;
    }

    @JsonProperty("heure_fin")
    public String getHeureFin() {
        return heureFin;
    }

    @JsonProperty("date_publication")
    public Date getPublishedAt() {
        return publishedAt;
    }

    @JsonProperty("date_enreg")
    public Date getCreated() {
        return created;
    }

    @JsonProperty("user_enreg")
    public Integer getCreator() {
        return creator;
    }

    @JsonProperty("user_maj")
    public Integer getUpdater() {
        return updater;
    }


}
