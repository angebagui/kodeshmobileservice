package controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import models.Enseignement;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.List;

/**
 * Created by angebagui on 24/01/2017.
 */
public class EnseignementsController extends Controller {

    public static final int TYPE_VIDEO = 3;
    public static final int TYPE_AUDIO = 2;
    public static final int TYPE_TEXT = 1;

    public Result getRessources(Integer limit){

        List<Enseignement> enseignements = Enseignement.finder.query().order().desc("id_parole").setMaxRows(limit).findList();
        ObjectNode jsonNode = Json.newObject();


        jsonNode.put("status_code", OK);
        jsonNode.put("status_message", "success");
        jsonNode.put("resources", Json.toJson(enseignements));
        return ok(jsonNode);
    }

    public Result getRessourceVideo(Integer limit){
        List<Enseignement> enseignements = Enseignement.finder.query().where().eq("id_type", TYPE_VIDEO).order().desc("id_parole").setMaxRows(limit).findList();
        ObjectNode jsonNode = Json.newObject();

        jsonNode.put("status_code", OK);
        jsonNode.put("status_message", "success");
        jsonNode.put("resources", Json.toJson(enseignements));
        return ok(jsonNode);
    }

    public Result getRessourceAudio(Integer limit){
        List<Enseignement> enseignements = Enseignement.finder.query().where().eq("id_type", TYPE_AUDIO).order().desc("id_parole").setMaxRows(limit).findList();
        ObjectNode jsonNode = Json.newObject();

        jsonNode.put("status_code", OK);
        jsonNode.put("status_message", "success");
        jsonNode.put("resources", Json.toJson(enseignements));
        return ok(jsonNode);
    }


    public Result getRessourceText(Integer limit){
        List<Enseignement> enseignements = Enseignement.finder.query().where().eq("id_type", TYPE_TEXT).order().desc("id_parole").setMaxRows(limit).findList();
        ObjectNode jsonNode = Json.newObject();

        jsonNode.put("status_code", OK);
        jsonNode.put("status_message", "success");
        jsonNode.put("resources", Json.toJson(enseignements));
        return ok(jsonNode);
    }

    public Result getEnseignementById(Long id){

        Enseignement enseignement = Enseignement.finder.where().eq("id_parole", id).findUnique();

        ObjectNode jsonNode = Json.newObject();

        jsonNode.put("status_code", OK);
        jsonNode.put("status_message", "success");
        jsonNode.put("resources", Json.toJson(enseignement));
        return ok(jsonNode);

    }




}
