package controllers;

import models.Live;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

/**
 * Created by angebagui on 24/01/2017.
 */
public class LivesController extends Controller {

    public Result getLives(){

        return ok(Json.toJson(Live.finder.findList()));
    }
}
