package controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import models.Evenement;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.List;


/**
 * Created by angebagui on 24/01/2017.
 */
public class EvenementsController extends Controller {


    public Result getEvenements(Integer limit){

        List<Evenement> evenemens = Evenement.finder.query().setMaxRows(limit).order().desc("id_evenement").findList();

        ObjectNode jsonNode = Json.newObject();

        jsonNode.put("status_code", OK);
        jsonNode.put("status_message", "success");
        jsonNode.put("resources", Json.toJson(evenemens));
        return ok(jsonNode);
    }

    public  Result getEvenementById(Long id){
        List<Evenement> evenemens = Evenement.finder.where().eq("id_evenement", id).findList();

        ObjectNode jsonNode = Json.newObject();

        jsonNode.put("status_code", OK);
        jsonNode.put("status_message", "success");
        jsonNode.put("evenement", Json.toJson(evenemens));
        return ok(jsonNode);

    }

}
