package controllers;

import models.Verse;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

/**
 * Created by angebagui on 24/01/2017.
 */
public class VersesController extends Controller {

    public Result getVerses(){
        return ok(Json.toJson(Verse.finder.all()));
    }
}
