package controllers;

import actors.message.*;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;
import models.*;
import play.data.FormFactory;
import play.libs.Json;
import play.libs.ws.WSClient;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Named;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


/**
 * Created by angebagui on 13/01/2017.
 */
public class AppController extends Controller {

    private final FormFactory formFactory;

    final ActorRef mPublisherActor;

    private ActorSystem actorSystem;

    private WSClient wsClient;

    @Inject
    public AppController(WSClient ws, @Named("publisher-actor")ActorRef publisherActor,   ActorSystem actorSystem,  FormFactory formFactory) {
        this.wsClient = ws;
        this.formFactory = formFactory;
        this.mPublisherActor = publisherActor;
        this.actorSystem = actorSystem;
    }


    public Result index(){
        List<Auteur> auteurs = Auteur.finder.findList();
        Live live = Live.finder.findList().get(0);
        return ok(views.html.dashboard.render(auteurs,live));
    }
    public Result login(){
        return ok(views.html.login.render());
    }

    public Result postLogin(){
        Admin admin = formFactory.form(Admin.class).bindFromRequest().get();
        return ok();
    }

    public Result dashboard(){
        List<Auteur> auteurs = Auteur.finder.findList();
        Live live = Live.finder.findList().get(0);
        return ok(views.html.dashboard.render(auteurs,live));
    }

    public Result messageList(){
        List<Message>  messages = Message.finder.where().order().desc("created").findList();
        Live live = Live.finder.findList().get(0);
        List<Auteur> auteurs = Auteur.finder.findList();

        return ok(views.html.messageList.render(messages,auteurs,live));
    }

    public Result deleteMessage(Long id){
        final Message message = Message.finder.where().eq("id", id).findUnique();

        if(message != null){
            message.delete();
        }
        return redirect(routes.AppController.messageList());
    }

    public Result messageSendPushNotification(Long id){
        Message enseignement = Message.finder.where().eq("id", id).findUnique();

        if(enseignement != null){

            actorSystem.eventStream().publish(new MessageEvent(enseignement), mPublisherActor);
        }

        ObjectNode objectNode = Json.newObject();
        objectNode.put("statusCode", 200);
        objectNode.put("statusMessage", "success");
        objectNode.put("message", "Push Notification  sent Successfully");

        return created(objectNode);
    }
    public Result verseList(){
        List<Verse> verses = Verse.finder.where().order().desc("created").findList();
        Live live = Live.finder.findList().get(0);
        List<Auteur> auteurs = Auteur.finder.findList();

        return ok(views.html.verseList.render(verses,auteurs,live));
    }
    public Result deleteVerse(Long id){
        Verse verse = Verse.finder.where().eq("id", id).findUnique();
        if(verse != null){
            verse.delete();
        }

        return redirect(routes.AppController.verseList());
    }
    public Result verseSendPushNotification(Long id){

        Verse enseignement = Verse.finder.where().eq("id", id).findUnique();

        if(enseignement != null){

            actorSystem.eventStream().publish(new VerseEvent(enseignement), mPublisherActor);
        }

        ObjectNode objectNode = Json.newObject();
        objectNode.put("statusCode", 200);
        objectNode.put("statusMessage", "success");
        objectNode.put("message", "Push Notification  sent Successfully");

        return created(objectNode);
    }


    public Result createVerse(){

        String titre = request().body().asFormUrlEncoded().get("titre")[0];
        String sousTitre = request().body().asFormUrlEncoded().get("sousTitre")[0];
        String description = request().body().asFormUrlEncoded().get("description")[0];
        String authorPhoto = request().body().asFormUrlEncoded().get("authorPhoto")[0];
        String authorName = request().body().asFormUrlEncoded().get("authorName")[0];
        String churchSession = request().body().asFormUrlEncoded().get("churchSession")[0];

        Verse verse = new Verse();
        verse.title = titre;
        verse.subTitle = sousTitre;
        verse.content = description;
        verse.authorName = authorName;
        verse.authorPhoto = authorPhoto;
        verse.isRead = false;
        verse.churchSession = churchSession;
        verse.created = System.currentTimeMillis();

        verse.save();

        actorSystem.eventStream().publish(new VerseEvent(verse), mPublisherActor);



        ObjectNode objectNode = Json.newObject();
        objectNode.put("statusCode", CREATED);
        objectNode.put("statusMessage", "success");
        objectNode.put("message", "Verse saved Successfully");

        return created(objectNode);
    }

    public Result editVerse(){


        String verseIdString = request().body().asFormUrlEncoded().get("verseId")[0];
        String titre = request().body().asFormUrlEncoded().get("titre")[0];
        String sousTitre = request().body().asFormUrlEncoded().get("sousTitre")[0];
        String description = request().body().asFormUrlEncoded().get("description")[0];
        String authorPhoto = request().body().asFormUrlEncoded().get("authorPhoto")[0];
        String authorName = request().body().asFormUrlEncoded().get("authorName")[0];

        Long verseId = Long.valueOf(verseIdString);

        Verse verse = Verse.finder.where().eq("id", verseId).findUnique();

        ObjectNode objectNode = Json.newObject();
        if (verse != null){

            verse.title = titre;
            verse.subTitle = sousTitre;
            verse.content = description;
            verse.authorName = authorName;
            verse.authorPhoto = authorPhoto;

            verse.created = System.currentTimeMillis();

            verse.update();

            objectNode.put("statusCode", OK);
            objectNode.put("statusMessage", "success");
            objectNode.put("message", "Verse updated Successfully");



        }



        return ok(objectNode);
    }

    public Result createMessage(){
        String titre = request().body().asFormUrlEncoded().get("titre")[0];
        String description = request().body().asFormUrlEncoded().get("content")[0];

        Message message = new Message();
        message.title = titre;
        message.content = description;
        message.save();

        actorSystem.eventStream().publish(new MessageEvent(message), mPublisherActor);


        ObjectNode objectNode = Json.newObject();
        objectNode.put("statusCode", CREATED);
        objectNode.put("statusMessage", "success");
        objectNode.put("message", "Message saved Successfully");

        return created(objectNode);
    }

    public Result editMessage(){
        String messageIdString = request().body().asFormUrlEncoded().get("messageId")[0];
        String titre = request().body().asFormUrlEncoded().get("title")[0];
        String description = request().body().asFormUrlEncoded().get("content")[0];

        Long messageId = Long.valueOf(messageIdString);
        Message message = Message.finder.where().eq("id", messageId).findUnique();

        ObjectNode objectNode = Json.newObject();
        if (message != null){
            message.title = titre;
            message.content = description;
            message.update();

            objectNode.put("statusCode", OK);
            objectNode.put("statusMessage", "success");
            objectNode.put("message", "Message updated Successfully");

            return ok(objectNode);

        }else {

            objectNode.put("statusCode", BAD_REQUEST);
            objectNode.put("statusMessage", "bad");
            objectNode.put("message", "Message bad request Successfully");

            return badRequest(objectNode);

        }


    }




    public Result enseignementSendPushNotification(Long id){
        Enseignement enseignement = Enseignement.finder.where().eq("id_parole", id).findUnique();

        if(enseignement != null){

            actorSystem.eventStream().publish(new LearningEvent(enseignement), mPublisherActor);
        }

        ObjectNode objectNode = Json.newObject();
        objectNode.put("statusCode", 200);
        objectNode.put("statusMessage", "success");
        objectNode.put("message", "Push Notification  sent Successfully");

        return created(objectNode);

    }


    public Result eventSendPushNotification(Long id){
        Evenement enseignement = Evenement.finder.where().eq("id_evenement", id).findUnique();

        if(enseignement != null){

            actorSystem.eventStream().publish(new AgendaEvent(enseignement), mPublisherActor);
        }

        ObjectNode objectNode = Json.newObject();
        objectNode.put("statusCode", 200);
        objectNode.put("statusMessage", "success");
        objectNode.put("message", "Push Notification  sent Successfully");

        return created(objectNode);

    }

    public Result createDevice(){
        String token  = request().body().asFormUrlEncoded().get("token")[0];

        if(token != null){
            Device device = Device.finder.where().eq("token", token).findUnique();

            if(device != null){


                ObjectNode objectNode = Json.newObject();
                objectNode.put("statusCode", 500);
                objectNode.put("statusMessage", "error");
                objectNode.put("message", "Already in the backend Server");

                return ok(objectNode);
            }else {

                device = new Device();

                device.token= token;
                device.save();

                ObjectNode objectNode = Json.newObject();
                objectNode.put("statusCode", 200);
                objectNode.put("statusMessage", "success");
                objectNode.put("message", "Added Successfully");

                return created(objectNode);

            }

        }else {

            ObjectNode objectNode = Json.newObject();
            objectNode.put("statusCode", 500);
            objectNode.put("statusMessage", "error");
            objectNode.put("message", "Token not found");

            return badRequest(objectNode);

        }


    }

    public Result eventList(){
        List<Evenement> evenements = Evenement.finder.where().order().desc("date_enreg").findList();
        Live live = Live.finder.findList().get(0);
        List<Auteur> auteurs = Auteur.finder.findList();
        return ok(views.html.evenementList.render(evenements, auteurs,live));
    }

    public Result editLive(){
        Live live = Live.finder.findList().get(0);

        if (live != null){
            String title = request().body().asFormUrlEncoded().get("titre")[0];
            String youtubeId = request().body().asFormUrlEncoded().get("youtubeId")[0];
            String url = request().body().asFormUrlEncoded().get("linkLive")[0];

            if(title != null){
                live.title = title;
            }

            if (youtubeId != null){
                live.youtubeId = youtubeId;
            }

            if (url != null){
                live.url = url;
            }

            live.update();

            actorSystem.eventStream().publish(new LiveEvent(live), mPublisherActor);

        }

        ObjectNode objectNode = Json.newObject();
        objectNode.put("statusCode", 200);
        objectNode.put("statusMessage", "success");
        objectNode.put("message", "Updated");

        return ok(objectNode);

    }

    public Result createEvent(){

        String titre = request().body().asFormUrlEncoded().get("titre")[0];
        String description = request().body().asFormUrlEncoded().get("description")[0];
        String image = request().body().asFormUrlEncoded().get("image")[0];
        String link = request().body().asFormUrlEncoded().get("link")[0];
        String dateRangeString = request().body().asFormUrlEncoded().get("dateRangeEvent")[0];

        String startTimeString = dateRangeString.split("-")[0];
        String endTimeString = dateRangeString.split("-")[1];



        //System.out.print("Date Range Event "+dateRangeString);

        final Evenement evenement = new Evenement();
        evenement.title = titre==null?"":titre;

        evenement.description = description==null?"":description;
        evenement.image = image;

        evenement.activer = true;
        evenement.publishedAt =  new Date();
        evenement.created = new Date();

        DateFormat formatter = new SimpleDateFormat("MM/dd/yy h:mm a");
        try {
            Date startTime = formatter.parse(startTimeString);
            if (startTime != null){
                evenement.startTime = startTime;
                evenement.heureDebut = startTime.getHours()<10?"0"+startTime.getHours():startTime.getHours()+":"+ (startTime.getMinutes()<10?""+startTime.getMinutes():startTime.getMinutes());
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            Date endTime = formatter.parse(endTimeString);

            if (endTime != null){
                evenement.endTime = endTime;
                evenement.heureFin = endTime.getHours()<10?"0"+endTime.getHours():endTime.getHours()+":"+ (endTime.getMinutes()<10?""+endTime.getMinutes():endTime.getMinutes());
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        evenement.save();

        ObjectNode objectNode = Json.newObject();
        objectNode.put("statusCode", 201);
        objectNode.put("statusMessage", "success");
        objectNode.put("message", "Created");

        actorSystem.eventStream().publish(new AgendaEvent(evenement), mPublisherActor);

        return created (objectNode);
    }

    public Result deleteEvent(Long id){
        Evenement enseignement = Evenement.finder.where().eq("id_evenement", id).findUnique();

        if(enseignement != null){
            enseignement.delete();
        }
        return redirect(routes.AppController.eventList());
    }

    public Result editEvent(){

        String eventId = request().body().asFormUrlEncoded().get("eventId")[0];
        String titre = request().body().asFormUrlEncoded().get("titre")[0];
        String description = request().body().asFormUrlEncoded().get("description")[0];
        String image = request().body().asFormUrlEncoded().get("image")[0];
        String link = request().body().asFormUrlEncoded().get("link")[0];
        String dateRangeString = request().body().asFormUrlEncoded().get("dateRangeEvent")[0];


        String startTimeString = dateRangeString.split("-")[0];
        String endTimeString = dateRangeString.split("-")[1];

        final Evenement evenement = Evenement.finder.where().eq("id_evenement", Long.valueOf(eventId)).findUnique();

        ObjectNode objectNode = Json.newObject();
        if (evenement != null){

            evenement.title = titre;

            evenement.description = description;
            evenement.image = image;

            evenement.activer = true;
            evenement.publishedAt =  new Date();
            evenement.created = new Date();

            evenement.save();


            objectNode.put("statusCode", 201);
            objectNode.put("statusMessage", "success");
            objectNode.put("message", "Updated");

            return created (objectNode);
        }else {

            objectNode.put("statusCode", 400);
            objectNode.put("statusMessage", "bad");
            objectNode.put("message", "Event not found, Please put correct id in your request");

            return badRequest(objectNode);
        }


    }

    public Result enseignementsList(){
        List<Enseignement> enseignements = Enseignement.finder.where().order().desc("date_enreg").findList();
        List<Auteur> auteurs = Auteur.finder.findList();
        Live live = Live.finder.findList().get(0);

        return ok(views.html.enseignementList.render(enseignements, auteurs,live));
    }



    public Result enseignementsListVideo(){
        List<Enseignement> enseignements = Enseignement.finder.where().eq("id_type", 3).order().desc("date_enreg").findList();
        List<Auteur> auteurs = Auteur.finder.findList();
        Live live = Live.finder.findList().get(0);

        return ok(views.html.enseignementList.render(enseignements, auteurs,live));
    }
    public Result enseignementsListAudio(){
        List<Enseignement> enseignements = Enseignement.finder.where().eq("id_type", 2).order().desc("date_enreg").findList();
        List<Auteur> auteurs = Auteur.finder.findList();
        Live live = Live.finder.findList().get(0);

        return ok(views.html.enseignementList.render(enseignements,auteurs,live));
    }

    public Result deleteEnseignement(Long id){
        Enseignement enseignement = Enseignement.finder.where().eq("id_parole", id).findUnique();

        if(enseignement != null){
            enseignement.delete();
        }
        return redirect(routes.AppController.enseignementsList());
    }

    public Result createEnseignement(){

        String titre = request().body().asFormUrlEncoded().get("titre")[0];
        String sousTitre = request().body().asFormUrlEncoded().get("sousTitre")[0];
        String description = request().body().asFormUrlEncoded().get("description")[0];
        String image = request().body().asFormUrlEncoded().get("image")[0];
        String link = request().body().asFormUrlEncoded().get("link")[0];
        String auteurString = request().body().asFormUrlEncoded().get("auteur")[0];
        String typeEnseignement = request().body().asFormUrlEncoded().get("typeEnseignement")[0];


        Long auteurId =  Long.valueOf(auteurString);
        Auteur auteur = Auteur.finder.where().eq("id_zoom", auteurId).findUnique();

        final Enseignement enseignement = new Enseignement();
        enseignement.title = titre;
        enseignement.subTitle = sousTitre;
        enseignement.description = description;
        enseignement.image = image;
        enseignement.urlMedia = link;
        enseignement.idType = Integer.valueOf(typeEnseignement);
        enseignement.activer = true;
        enseignement.publishedAt =  new Date();
        enseignement.auteur = auteur != null ? auteur.code : null;
        enseignement.created = new Date();
        enseignement.updated = new Date();
        enseignement.save();

        ObjectNode objectNode = Json.newObject();
        objectNode.put("statusCode", 201);
        objectNode.put("statusMessage", "success");
        objectNode.put("message", "Created");

        actorSystem.eventStream().publish(new LearningEvent(enseignement), mPublisherActor);

        return created (objectNode);
    }


    public Result editEnseignement(){

        String enseignementId = request().body().asFormUrlEncoded().get("enseignementId")[0];
        String titre = request().body().asFormUrlEncoded().get("titre")[0];
        String sousTitre = request().body().asFormUrlEncoded().get("sousTitre")[0];
        String description = request().body().asFormUrlEncoded().get("description")[0];
        String image = request().body().asFormUrlEncoded().get("image")[0];
        String link = request().body().asFormUrlEncoded().get("link")[0];
        String auteurString = request().body().asFormUrlEncoded().get("auteur")[0];
        String typeEnseignement = request().body().asFormUrlEncoded().get("typeEnseignement")[0];

        Long auteurId =  Long.valueOf(auteurString);
        Auteur auteur = Auteur.finder.where().eq("id_zoom", auteurId).findUnique();


        Enseignement enseignement = Enseignement.finder.where().eq("id_parole", Long.valueOf(enseignementId)).findUnique();

        ObjectNode objectNode = Json.newObject();
        if(enseignement != null){
        enseignement.title = titre;
        enseignement.subTitle = sousTitre;
        enseignement.description = description;
        enseignement.activer = true;
        enseignement.image = image;
        enseignement.urlMedia = link;
        enseignement.idType = Integer.valueOf(typeEnseignement);
        enseignement.auteur = auteur != null ? auteur.code : null;
        enseignement.updated = new Date();
        enseignement.update();


        objectNode.put("statusCode", 201);
        objectNode.put("statusMessage", "success");
        objectNode.put("message", "Updated");

        objectNode.put("statusCode", 201);
        objectNode.put("statusMessage", "success");
        objectNode.put("message", "Updated");

        return created (objectNode);
    }else {

        objectNode.put("statusCode", 400);
        objectNode.put("statusMessage", "bad");
        objectNode.put("message", "Enseignement not found, Please put correct id in your request");

        return badRequest(objectNode);
    }
    }



}
