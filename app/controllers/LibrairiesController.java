package controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import models.Librairie;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.List;

/**
 * Created by angebagui on 24/01/2017.
 */
public class LibrairiesController extends Controller {

    public Result getLibrairies(Integer limit){
        final List<Librairie> librairies = Librairie.finder.query().order().desc("id_librairie").setMaxRows(limit).findList();

        ObjectNode jsonNode = Json.newObject();

        jsonNode.put("status_code", OK);
        jsonNode.put("status_message", "success");
        jsonNode.put("resources", Json.toJson(librairies));
        return ok(jsonNode);
    }

    public Result getLibrairieById(Long id){

        Librairie librairie = Librairie.finder.where().eq("id_librairie", id).findUnique();

        ObjectNode jsonNode = Json.newObject();

        jsonNode.put("status_code", OK);
        jsonNode.put("status_message", "success");
        jsonNode.put("librairie", Json.toJson(librairie));
        return ok(jsonNode);

    }
}
