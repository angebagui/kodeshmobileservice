package services;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import play.libs.Json;
import play.libs.ws.*;

import java.util.concurrent.CompletionStage;

/**
 * Created by angebagui on 22/01/2017.
 */
public class GcmService {

    // Google Cloud Messaging Config
    // val API_URL = "https://gcm-http.googleapis.com/gcm/send"
    //val API_KEY = "AIzaSyA2SrWzs0wjiKj3bi9_xeRz5KvKSse2E4s"

    // Firebase Cloud Message Config
    public static String API_URL = "https://fcm.googleapis.com/fcm/send";
    public static String API_KEY = "AAAAgoD2OUU:APA91bEAepwsQfJlyUlEaBBqyFwTt_kx2_6wWsN0Hq9JJqinUDCXjkYnq4g3nUS1JG4IlmKlbAVNzlXNxT5auvgRu2F3hpZ3046lQISUXQsdfpdwA1hAd6atZLbxTaNeUvsdRJqq35VE";



   public static CompletionStage<WSResponse> sendPayloadWithNotification(WSClient ws, String title, String text, String to) {
        ObjectNode notification = Json.newObject();

        notification.put("title", title);
        notification.put("text", text);


       ObjectNode requestBody = Json.newObject();
       requestBody.put("notification", notification);
       requestBody.put("to", to);


       WSRequest request= ws.url(API_URL).setHeader(
                "Authorization" , "key=".concat(API_KEY))
                .setHeader( "Content-Type" , "application/json");

        return request.post(requestBody);
    }
    public static CompletionStage<WSResponse> sendPayloadWithData(WSClient ws, JsonNode data, String to){

        ObjectNode requestBody =   Json.newObject();

        requestBody.put("data",data);
        requestBody.put("to", to);


        WSRequest request= ws.url(API_URL).setHeader(
                "Authorization" , "key=".concat(API_KEY))
                .setHeader( "Content-Type" , "application/json");

        return request.post(requestBody);
    }
}
